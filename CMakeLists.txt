﻿# CMakeList.txt : CMake project for NewProt, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

project ("NewProt")

# Add source to this project's executable.
add_executable (NewProt "NewProt.cpp" "NewProt.h")

# TODO: Add tests and install targets if needed.
